WINAPP = (function() {
  // Demo 1 with callbacks
  let calendar = new WINSTONCAL(document.getElementById('calendar'), {
    onMonthChange: (prevMonth, currentMonth) => {
      console.log('prevMonth ', prevMonth);
      console.log('currentMonth ', currentMonth)
    },
    onSelect: (date) => console.log(date)
  });

  // Demo 2 with spanish months
  let calendar2 = new WINSTONCAL(document.getElementById('calendar2'), {
    language: 'spanish'
  });
})();