/*
 instructions: https://notehub.org/jpxki
 */

WINSTONCAL = (function() {
  class Calendar {
    constructor(calendarEl, options) {
      this.options = Object.assign({
        onMonthChange: () => undefined,
        onSelect: () => undefined,
        language: 'english'
      }, options);

      // Some error handling
      if (!calendarEl) {
        throw new Error('Please pass in calendar document reference');
      }

      if (this.options.onMonthChange && (typeof this.options.onMonthChange !== 'function')) {
        throw new Error('onMonthChange should be a function');
      }

      if (this.options.onSelect && typeof this.options.onSelect !== 'function') {
        throw new Error('onSelect should be a function');
      }

      this.MONTH_DAYS = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12",
        "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24",
        "25", "26", "27", "28", "29", "30", "31"];
      this.MONTH_NAMES = MONTH_NAMES[this.options.language];
      this.startDayOfWeek = 1;
      this.currentDate = new Date();
      this.currentYear = this.currentDate.getFullYear();
      this.currentMonth = this.currentDate.getMonth();
      this.prevMonth = this.currentMonth;
      this.currentMonthName = this.MONTH_NAMES[this.currentDate.getMonth()];

      this.setup(calendarEl);
    }

    setup(calendarEl) {
      this.calendarEl = calendarEl;
      let template  = `
        <div class="calendar__header">
          <span class="calendar__prevMonth"> &#171; </span>
          <span class="calendar__monthYear"></span>
          <span class="calendar__nextMonth"> &#187; </span>
         </div>
         <div class="calendar__cells"></div>`;

      this.calendarEl.innerHTML = template;
      this.calendarCells = this.calendarEl.querySelector('.calendar__cells');

      // onMonthChange handlers
      this.calendarEl.querySelector('.calendar__prevMonth')
        .addEventListener('click', () => {
          this.goToMonth('prev');
          this.options.onMonthChange(this.prevMonth, this.currentMonth);
        });

      this.calendarEl.querySelector('.calendar__nextMonth')
        .addEventListener('click', () => {
          this.goToMonth('next');
          this.options.onMonthChange(this.prevMonth, this.currentMonth);
        });
      
      // onSelect handler
      this.calendarEl.querySelector('.calendar__cells')
        .addEventListener('click', (e) => {
          let day;

          if (e.target.className !== 'calendar__cell--day') {
            return;
          }

          day = Number(e.target.textContent);
          this.options.onSelect(new Date(this.currentYear, this.currentMonth, day));
        });

      // Render calendar
      this.renderCalendar();
    }

    renderCalendar() {
      let emptyCellText = '';
      let docFrag = document.createElement('div');
      this.startDayOfWeek = UTILS.getStartDayOfWeek(this.currentYear, this.currentMonth);
      let lastDayOfMonth = UTILS.getLastDayOfMonth(this.currentYear, this.currentMonth);

      // Update current month year text
      this.calendarEl.querySelector('.calendar__monthYear')
        .textContent = this.currentMonthName + ' ' + this.currentYear;

      // Generate days array
      let beforeArr = UTILS.getFillerDays(0, this.startDayOfWeek, emptyCellText);
      let dayArr = UTILS.getDays(this.MONTH_DAYS, this.startDayOfWeek, this.startDayOfWeek + lastDayOfMonth);
      let afterArr = UTILS.getFillerDays(this.startDayOfWeek + lastDayOfMonth, lastDayOfMonth, emptyCellText);
      let allDays = beforeArr.concat(dayArr, afterArr);

      // Chunk days into weeks array
      let weeks = UTILS.chunkedWeeks(allDays, 7);

      // Generate divs from weeks array
      weeks.forEach(week => {
        let weekEl = document.createElement('div');

        // For each week generate day divs
        week.forEach(day => {
          let dayEl = this.composeDayEl(day);
          weekEl.appendChild(dayEl);
        });
        docFrag.appendChild(weekEl);
      });

      this.calendarCells.innerHTML = '';
      this.calendarCells.appendChild(docFrag);
    }

    composeDayEl(text) {
      let calendarCell = document.createElement('div');
      let calendarCellDay = document.createElement('div');
      calendarCellDay.textContent = text;
      calendarCell.appendChild(calendarCellDay);

      if (text) {
        calendarCellDay.className = 'calendar__cell--day';
      } else {
        // Filler/blank elements to keep days aligned to M,T,W,TH,F,S,SU
        calendarCellDay.className = 'calendar__cell--filler';
      }

      calendarCell.className = 'calendar__cell';

      return calendarCell;
    }

    goToMonth(direction) {
      this.prevMonth = this.currentDate.getMonth();

      if (direction === 'next') {
        this.currentDate = UTILS.incrementMonth(this.currentYear, this.prevMonth);
      } else if(direction === 'prev') {
        this.currentDate = UTILS.decrementMonth(this.currentYear, this.prevMonth);
      }

      this.currentYear = this.currentDate.getFullYear();
      this.currentMonth = this.currentDate.getMonth();
      this.currentMonthName = this.MONTH_NAMES[this.currentMonth];

      this.renderCalendar();
    }
  }

  return Calendar;
})();