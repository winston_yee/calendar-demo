UTILS = (function() {
  function decrementMonth(currentYear, lastMonth) {
    return new Date(currentYear, lastMonth - 1);
  }

  function incrementMonth(currentYear, lastMonth) {
    return new Date(currentYear, lastMonth + 1);
  }

  function getStartDayOfWeek(currentYear, currentMonth) {
    let dayOfMonth = new Date(currentYear, currentMonth , 1);
    return dayOfMonth.getDay();
  }

  function getLastDayOfMonth(currentYear, currentMonth) {
    let dayOfMonth = new Date(currentYear, currentMonth + 1 , 0);
    return dayOfMonth.getDate();
  }

  function getFillerDays(start, end, text) {
    let cells = [];

    for (let i=start; i<end; i++) {
      cells.push(text);
    }

    return cells;
  }

  function getDays(MONTH_DAYS, start, end) {
    let cells = [];
    let day = 0;

    for (let i=start; i<end; i++) {
      cells.push(MONTH_DAYS[day]);
      day++;
    }

    return cells;
  }

  function chunkedWeeks(days, chunk) {
    let weeks = [];

    while(days.length) {
      weeks.push(days.splice(0, chunk));
    }

    return weeks;
  }

  return {
    decrementMonth: decrementMonth,
    incrementMonth: incrementMonth,
    getStartDayOfWeek: getStartDayOfWeek,
    getLastDayOfMonth: getLastDayOfMonth,
    getFillerDays: getFillerDays,
    getDays: getDays,
    chunkedWeeks: chunkedWeeks
  }
})();